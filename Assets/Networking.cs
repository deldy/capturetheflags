using UnityEngine;
using System.Collections;

public class Networking : MonoBehaviour {
	
	private string gameName = "Deldysoft_CaptureTheFlags";
	private HostData[] hosts = new HostData[0];
	
	public Transform SpawnObject;
	public GameObject PlayerPrefab;
	
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}
	
	void OnGUI()
	{
		if(!Network.isServer && !Network.isClient)
		{
			if(GUI.Button (new Rect(0,0,50,50), "Start"))
			{
				Network.InitializeServer(4, 25001, !Network.HavePublicAddress());
				MasterServer.RegisterHost(gameName, "Capture the Flags!", "This is a prototype");
			}
			
			if(GUI.Button (new Rect(50,0,50,50), "Servers"))
			{
				MasterServer.RequestHostList (gameName);
			}
			
			for(int x = 0; x< hosts.Length; x++)
			{
				HostData host = hosts[x];
				if(GUI.Button (new Rect(x*50, 50, 50, 50), host.gameName))
				{
					Network.Connect (host);
				}
			}
		}
	}
	
	
	
	void OnServerInitialized()
	{
		Debug.Log ("Server started");
		spawnPlayer();
	}
	
	void spawnPlayer()
	{
		Network.Instantiate(PlayerPrefab, SpawnObject.position, Quaternion.identity, 0);
	}
	
	void OnConnectedToServer()
	{
		spawnPlayer();
		
	}
	
	
	void OnMasterServerEvent(MasterServerEvent mse)
	{
		if(mse == MasterServerEvent.RegistrationSucceeded)
		{
			Debug.Log ("You are now public");
		}
		else if(mse == MasterServerEvent.HostListReceived)
		{
			hosts = MasterServer.PollHostList();
		}
	}
}
