using UnityEngine;
using System.Collections;

public class PlayerScript : MonoBehaviour
{
	public bool GotFlag = false;
    public float Speed;
	public float ShotSpeed;
	public GameObject ShotPrefab;
	// Use this for initialization
	void Start () {
	}
	
	// Update is called once per frame
	void Update () {
		if(networkView.isMine)
		{
			rigidbody.AddForce(new Vector2(Input.GetAxis("Horizontal")*Speed, Input.GetAxis("Vertical")*Speed));
			
			if(Input.GetMouseButtonDown (0))
			{
				if(ShotPrefab != null)
				{
					if(Camera.main != null)
					{
						var pos = Input.mousePosition;
						pos.z = -Camera.main.transform.position.z;
						var position = Camera.main.ScreenToWorldPoint(pos);
						position.z = 0;
						
						var obj = (GameObject) Network.Instantiate(ShotPrefab, this.transform.position, Quaternion.identity, 0);
						
						var direction = position - obj.transform.position;
						obj.rigidbody.AddForce(direction.normalized * ShotSpeed);
						Debug.Log ("spawned");
						
					}
				}
			}
		} else
		{
			enabled = false;
		}
	}
	
	void OnCollisionEnter(Collision collision) {
        foreach (ContactPoint contact in collision.contacts) {
			if(contact.otherCollider.gameObject.name == "Sphere")
			{
				if(GotFlag)
				{
					Debug.Log("WIN!");
				}
				else
				{
					contact.otherCollider.gameObject.transform.parent = this.transform;
					GotFlag = true;
					contact.otherCollider.enabled = false;
				}
			}
        }
	}
	
	
}
